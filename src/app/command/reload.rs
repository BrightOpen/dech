use crate::{app::command::*, app::Dech, dech_cmd2};
use anyhow::Result;
use log::*;

dech_cmd2! {Reload}
impl Reload {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        info!("reloading");

        let mut profiles = vec![];
        for account in dech.backend_mut().await.get_all_accounts()? {
            profiles.push(account.load_info().await)
        }

        let account_id = dech
            .backend_mut()
            .await
            .get_selected_account_id()
            .or_else(|| profiles.iter().map(|p| p.id).next());

        dech.profiles = profiles;
        match account_id {
            Some(account_id) => SelectAccount { account_id }.run(dech).await?,
            None => {
                dech.session = None;
                dech.conversation = None;
            }
        };

        Ok(())
    }
}
