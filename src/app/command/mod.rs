mod create_or_update_account;
mod delete_account;
mod handle_event;
mod refresh_account;
mod refresh_chat;
mod reload;
mod save_draft;
mod select_account;
mod select_chat;
mod send_draft;
mod send_text_message;
mod unselect_account;
mod unselect_chat;

pub use create_or_update_account::*;
pub use delete_account::*;
pub use handle_event::*;
pub use refresh_account::*;
pub use refresh_chat::*;
pub use reload::*;
pub use save_draft::*;
pub use select_account::*;
pub use select_chat::*;
pub use send_draft::*;
pub use send_text_message::*;
pub use unselect_account::*;
pub use unselect_chat::*;

use crate::app::Dech;
use anyhow::Result;
use futures::Future;
use std::pin::Pin;

pub trait Command: std::fmt::Debug + std::fmt::Display  {
    fn start<'a>(&'a self, dech: &'a mut Dech) -> Pin<Box<dyn Future<Output = Result<()>> + 'a>>;
}

impl<T> Command for Box<T>
where
    T: Command,
{
    fn start<'a>(&'a self, dech: &'a mut Dech) -> Pin<Box<dyn Future<Output = Result<()>> + 'a>> {
        T::start(self, dech)
    }
}

#[macro_export]
macro_rules! dech_cmd2 {
    // struct with fields
    (
        $name:ident {$($field:ident : $field_type:ty),+}
    ) => {$crate::dech_cmd2!{$name
                    ({ $(pub $field : $field_type ,)+ })
        }};

    // struct without fields
    (
        $name:ident
    ) => {$crate::dech_cmd2!{$name
                    (;)
        }};

    // internal monster rule
    (
        $name:ident
        ($body:tt)
    ) => {
        #[derive(::std::fmt::Debug, Clone)]
        pub struct $name $body

        impl $crate::app::command::Command for $name {
            fn start<'a>(
                &'a self,
                dech: &'a mut $crate::app::Dech,
            ) -> ::std::pin::Pin<
                    ::std::boxed::Box<
                        dyn ::std::future::Future<
                            Output =
                                ::anyhow::Result<()>
                        > + 'a
                    >
                >
            {
                let me = self.clone();

                Box::pin(async move {
                     me.run(dech).await?;
                     Ok(())
                })
            }
        }

        impl ::std::fmt::Display for $name {
            fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>)->::std::fmt::Result{
                f.write_str(stringify!($name))
            }
        }
    };
}
