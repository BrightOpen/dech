use crate::{app::Dech, dech_cmd2};
use anyhow::Result;
use log::info;

dech_cmd2! {UnselectChat}

impl UnselectChat {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        info!("unselecting chat");
        dech.conversation = None;
        Ok(())
    }
}
