use crate::{app::Dech, dech_cmd2};
use anyhow::Result;
use log::*;

dech_cmd2!(DeleteAccount { account_id: u32 });

impl DeleteAccount {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        info!("deleting account {}", self.account_id);
        dech.backend_mut()
            .await
            .remove_account(self.account_id)
            .await?;
        dech.profiles.retain(|p| p.id != self.account_id);
        dech.conversation = None;
        dech.session = None;
        Ok(())
    }
}
