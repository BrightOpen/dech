use crate::{app::command::*, app::Dech, dech_cmd2};
use anyhow::Result;
use log::*;

dech_cmd2! {HandleEvent {account_id: u32, kind: deltachat::EventType}}

impl HandleEvent {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        let Self { account_id, kind } = self;
        use deltachat::EventType as E;
        Ok(match kind {
            E::Info(msg) => debug!("[{}] {}", account_id, msg),
            E::Error(msg) => error!("[{}] {}", account_id, msg),
            E::Warning(msg) => warn!("[{}] {}", account_id, msg),

            E::ConnectivityChanged => {
                info!("[{}] connectivity changed", account_id);
                RefreshAccount { account_id }.run(dech).await?
            }
            E::SmtpConnected(msg) => {
                info!("[{}] SMTP connected - {}", account_id, msg);
                RefreshAccount { account_id }.run(dech).await?
            }
            E::ImapConnected(msg) => {
                info!("[{}] IMAP connected - {}", account_id, msg);
                RefreshAccount { account_id }.run(dech).await?
            }
            E::ConfigureProgress { progress, comment } => {
                debug!(
                    "[{}] Configure progress {}! comment: {:?}",
                    account_id, progress, comment
                );
                RefreshAccount { account_id }.run(dech).await?
            }
            E::ImexProgress(progress) => {
                debug!("[{}] Imex progress {}!", account_id, progress);
                RefreshAccount { account_id }.run(dech).await?
            }

            E::MsgDelivered { chat_id, .. }
            | E::MsgFailed { chat_id, .. }
            | E::MsgsChanged { chat_id, .. }
            | E::MsgRead { chat_id, .. }
            | E::ChatModified(chat_id)
            | E::MsgsNoticed(chat_id) => {
                info!("[{}] Message updates for {}", account_id, chat_id);
                let chat_id = chat_id.to_u32();
                RefreshChat {
                    account_id,
                    chat_id,
                }
                .run(dech)
                .await?
            }

            e => debug!("[{}] DC Event {:?}", account_id, e),
        })
    }
}
