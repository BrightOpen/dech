use crate::{app::command::*, app::Dech, dech_cmd2};
use anyhow::Result;
use log::*;

dech_cmd2! {SelectAccount {account_id: u32}}

impl SelectAccount {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        let Self { account_id } = self;
        info!("selecting account {}", account_id);

        dech.backend_mut().await.select_account(account_id).await?;

        RefreshAccount { account_id }.run(dech).await
    }
}
