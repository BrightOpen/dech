use crate::{
    app::command::SelectAccount,
    app::Dech,
    backend::{types::ProfileConfig},
    dech_cmd2,
};
use anyhow::Result;
use log::*;

dech_cmd2! {CreateOrUpdateAccount { config: ProfileConfig}}

impl CreateOrUpdateAccount {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        let Self { config } = self;

        info!(
            "creating or updating an account id {:?} addr {}",
            config.account_id, config.primary_address
        );

        let account = match config.account_id {
            Some(account_id) => dech.backend_mut().await.account(account_id)?,
            None => dech.backend_mut().await.add_account().await?,
        };
        account.update_config(&config).await?;
        account.configure().await?;

        SelectAccount {
            account_id: account.id(),
        }
        .run(dech)
        .await
    }
}
