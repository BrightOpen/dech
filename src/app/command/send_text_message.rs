use crate::{app::Dech, dech_cmd2};
use anyhow::Result;
use log::info;

dech_cmd2! {SendTextMessage {account_id: u32, chat_id: u32, message: String}}

impl SendTextMessage {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        let Self {
            account_id,
            chat_id,
            message,
        } = self;

        info!("sending text for {}/{}", account_id, chat_id);

        dech.backend_mut()
            .await
            .account(account_id)?
            .chat(chat_id)
            .send_text_message(message)
            .await?;
        Ok(())
    }
}
