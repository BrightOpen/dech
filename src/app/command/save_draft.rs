use crate::{
    app::Dech,
    backend::{types::MessageContent},
    dech_cmd2,
};
use anyhow::Result;
use log::*;

dech_cmd2! {SaveDraft { account_id: u32, chat_id: u32, message: MessageContent } }

impl SaveDraft {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        let Self {
            account_id,
            chat_id,
            mut message,
        } = self;

        info!("saving draft for {}/{}", account_id, chat_id);

        let chat = dech.backend_mut().await.account(account_id)?.chat(chat_id);
        let mut draft = chat.draft().await?;

        if let Some(ref txt) = message.text {
            if !txt.is_empty() {
                draft.save(&mut message).await?;
            } else {
                draft.delete(&mut message).await?;
            }
        } else {
            draft.delete(&mut message).await?;
        }

        Ok(())
    }
}
