use crate::{app::Dech, dech_cmd2};
use anyhow::Result;
use log::info;

dech_cmd2! {UnselectAccount}

impl UnselectAccount {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        info!("unselecting account");
        dech.conversation = None;
        dech.session = None;
        Ok(())
    }
}
