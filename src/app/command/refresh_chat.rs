use crate::{app::Dech, dech_cmd2};
use anyhow::Result;
use log::*;

dech_cmd2! {RefreshChat {account_id: u32, chat_id: u32}}

impl RefreshChat {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        let Self {
            account_id,
            chat_id,
        } = self;

        info!("refreshing chat {}/{}", account_id, chat_id);

        let account = dech.backend_mut().await.account(account_id)?;
        let chat = account.chat(chat_id);
        let info = chat.load_info().await?;

        let conversation = match Some(chat_id) == account.get_selected_chat_id() {
            true => Some(chat.load(None).await?),
            false => None,
        };

        if let Some(account_state) = &mut dech.session {
            if let Some(target) = account_state
                .topics
                .iter_mut()
                .filter(|c| c.id == chat_id)
                .next()
            {
                *target = info;
            } else {
                account_state.topics.insert(0, info);
            }
        }
        if let Some(conversation) = conversation {
            dech.conversation = Some(conversation)
        }
        Ok(())
    }
}
