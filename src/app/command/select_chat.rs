use crate::{app::command::*, app::Dech, dech_cmd2};
use anyhow::Result;
use log::*;

dech_cmd2! {SelectChat {account_id: u32, chat_id: u32}}

impl SelectChat {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        let Self {
            account_id,
            chat_id,
        } = self;

        info!("selecting chat {}/{}", account_id, chat_id);

        dech.backend_mut()
            .await
            .select_chat(account_id, chat_id)
            .await?;

        RefreshChat {
            account_id,
            chat_id,
        }
        .run(dech)
        .await
    }
}
