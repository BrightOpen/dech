use crate::{app::Dech, dech_cmd2};
use anyhow::Result;
use log::*;

dech_cmd2! {RefreshAccount { account_id: u32}}

impl RefreshAccount {
    pub async fn run(self, dech: &mut Dech) -> Result<()> {
        let Self { account_id } = self;
        info!("refreshing account {}", account_id);

        let account = dech.backend_mut().await.account(account_id)?;
        let info = account.load_info().await;

        let session = match Some(account_id) == dech.backend_mut().await.get_selected_account_id() {
            true => Some(account.load(None).await?),
            false => None,
        };

        let conversation = match account.get_selected_chat_id() {
            Some(chat_id) => Some(account.chat(chat_id).load(None).await?),
            None => None,
        };

        if let Some(target) = dech
            .profiles
            .iter_mut()
            .filter(|a| a.id == self.account_id)
            .next()
        {
            *target = info;
        } else {
            dech.profiles.insert(0, info);
        }

        if let Some(session) = session {
            dech.session = Some(session);
        }
        if let Some(conversation) = conversation {
            dech.conversation = Some(conversation);
        }
        Ok(())
    }
}
