pub mod command;

use crate::{
    app::command::{Command, Reload},
    backend::{
        types::{Conversation, MessageContent, Profile, ProfileConfig, Session},
        Backend,
    },
    widgets::*,
};
use anyhow::Context;
use core::fmt;
use deltachat::Event;
use futures::StreamExt;
use log::*;
use std::collections::VecDeque;
use std::fmt::Debug;
use termit::{widget::Widget, Terminal};

pub struct Dech {
    /// Pending command queue
    commands: VecDeque<Box<dyn Command>>,
    /// DeltaChat backend state
    backend2: Option<Backend>,

    pub profiles: Vec<Profile>,
    pub session: Option<Session>,
    pub conversation: Option<Conversation>,
    pub new_config: ProfileConfig,
    pub search: String,
    pub is_terminating: bool,
    pub show_menu: bool,
    pub menu_page_width: u16,
    pub generation: usize,
    pub main_view: Box<dyn Widget<Dech, DechEvent>>,
    pub main_preview: Option<Box<dyn Widget<Dech, DechEvent>>>,
}

#[derive(Debug)]
pub enum DechEvent {
    NoOp,
    Backend(Event),
}
impl Default for DechEvent {
    fn default() -> Self {
        DechEvent::NoOp
    }
}
impl PartialEq for DechEvent {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Backend(dce), Self::Backend(dce2)) => dce == dce2,
            _ => core::mem::discriminant(self) == core::mem::discriminant(other),
        }
    }
}

impl Default for Dech {
    fn default() -> Self {
        let me = Self {
            show_menu: true,
            menu_page_width: 40,
            backend2: None,

            search: Default::default(),
            is_terminating: Default::default(),
            commands: Default::default(),
            generation: Default::default(),
            main_view: Box::new(make_ui_main_view_welcome()),
            main_preview: None,
            new_config: Default::default(),
            profiles: Default::default(),
            session: Default::default(),
            conversation: Default::default(),
        };
        me
    }
}

impl Dech {
    pub async fn run_tui(&mut self) -> anyhow::Result<()> {
        let mut ui = make_ui();
        let backend = Backend::start(None).await?;
        let dcevents = backend.get_event_emitter();
        self.backend2 = Some(backend);
        self.send_command(Reload);

        let mut termit = Terminal::try_system_default()?
            .into_termit()
            .enter_raw_mode()?
            .use_alternate_screen(true)?
            .capture_mouse(true)?
            .enable_line_wrap(false)?
            .show_cursor(false)?
            .handle_focus_events(true)?
            .include_app_input(dcevents.map(|dce| DechEvent::Backend(dce)));

        while !self.is_terminating && self.backend2.is_some() {
            let _gen = self.generation;
            // fail dramatically for now so that errors are noticed
            self.run_next_cmd().await?;
            termit.step(self, &mut ui).await?;
        }

        Ok(())
    }

    async fn run_next_cmd(&mut self) -> anyhow::Result<()> {
        while let Some(cmd) = self.commands.pop_front() {
            let desc = cmd.to_string();
            self.generation = self.generation.wrapping_add(1);
            debug!(
                "Started cmd {desc}, {} to go, gen {}",
                self.commands.len(),
                self.generation
            );
            cmd.start(self)
                .await
                .context(format!("running command {desc}"))?;
        }
        Ok(())
    }
    pub async fn backend_mut(&mut self) -> &mut Backend {
        match &mut self.backend2 {
            Some(backend) => backend,
            empty @ None => {
                let backend = Backend::start(None).await.expect("could not start backend");
                empty.insert(backend)
            }
        }
    }
    pub fn send_command(&mut self, command: impl Command + 'static) {
        debug!(
            "Queueing cmd {}. {} in total",
            command,
            self.commands.len() + 1
        );
        self.commands.push_back(Box::new(command));
    }
    pub fn draft(&mut self) -> Option<Draft> {
        self.session.as_ref().and_then(|s| {
            self.conversation
                .as_mut()
                .filter(|c| c.topic.can_send)
                .map(|c| Draft::new(s.profile.id, c.topic.id, &mut c.draft))
        })
    }
    pub fn profile_config_mut(&mut self) -> &mut ProfileConfig {
        self.session
            .as_mut()
            .map(|s| &mut s.config)
            .unwrap_or_else(|| &mut self.new_config)
    }
}

impl fmt::Debug for Dech {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Dech")
            .field("commands", &self.commands)
            .field("profiles", &self.profiles)
            .field("session", &self.session)
            .field("conversation", &self.conversation)
            .field("new_config", &self.new_config)
            .field("search", &self.search)
            .field("is_terminating", &self.is_terminating)
            .field("show_menu", &self.show_menu)
            .field("menu_page_width", &self.menu_page_width)
            .field("generation", &self.generation)
            .field("main_preview", &"Opt<Box(dyn Widget<Dech>)>")
            .field("main_view", &"Box(dyn Widget<Dech>)")
            .finish()
    }
}

#[derive(Debug)]
pub struct Draft<'a> {
    pub account_id: u32,
    pub chat_id: u32,
    pub message: &'a mut MessageContent,
}

impl<'a> Draft<'a> {
    pub fn new(account_id: u32, chat_id: u32, message: &'a mut MessageContent) -> Self {
        Self {
            account_id,
            chat_id,
            message,
        }
    }
}
