use super::{
    chat::ChatBackend,
    types::{NetSecurity, Profile, ProfileConfig, Session},
};
use crate::backend::types::Login;
use anyhow::{anyhow, bail, ensure, Context as _, Result};
use deltachat::{chatlist::Chatlist, config::Config, context::Context, EventType};
use futures::StreamExt;
use log::*;
use std::path::Path;

#[derive(Debug, Clone)]
pub struct AccountBackend(Context, Option<u32>);

impl AccountBackend {
    pub fn new(context: Context, selected_chat_id: Option<u32>) -> Self {
        Self(context, selected_chat_id)
    }
    fn context(&self) -> &Context {
        &self.0
    }
    pub async fn load_info(&self) -> Profile {
        let email = self
            .context()
            .get_config(Config::Addr)
            .await
            .unwrap()
            .unwrap_or_default();
        let display_name = self
            .context()
            .get_config(Config::Displayname)
            .await
            .unwrap();
        let profile_image = self
            .context()
            .get_config(Config::Selfavatar)
            .await
            .unwrap()
            .map(Into::into);
        let progress = self.context().get_connectivity().await as usize;
        let logged_in = match progress {
            0..=1999 => Login::Error(format!("{}", progress)),
            2000..=2999 => Login::Not,
            3000..=3999 => Login::Progress(progress),
            _ => Login::Success,
        };
        let id = self.context().get_id();

        Profile {
            id,
            logged_in,
            email,
            profile_image,
            display_name,
        }
    }
    pub async fn import(&self, path: &Path) -> Result<()> {
        use deltachat::imex;

        imex::imex(self.context(), imex::ImexMode::ImportBackup, path, None)
            .await
            .map_err(|err| anyhow!("{}", err))?;

        let mut events = self.context().get_event_emitter();
        while let Some(event) = events.next().await {
            match event.typ {
                EventType::ImexProgress(0) => {
                    bail!("Failed to import");
                }
                EventType::ImexProgress(1000) => {
                    self.context().start_io().await;
                    break;
                }
                _ => {}
            }
        }

        Ok(())
    }

    pub async fn update_config(&self, config: &ProfileConfig) -> Result<()> {
        let ProfileConfig {
            account_id,
            display_name,
            primary_address,
            secondary_addresses,
            mail_server,
            mail_port,
            mail_user,
            mail_password,
            mail_security,
            send_server,
            send_port,
            send_user,
            send_password,
            send_security,
        } = &config;

        if let Some(account_id) = account_id {
            ensure!(*account_id == self.context().get_id());
        }

        fn optional_str(s: &str) -> Option<&str> {
            if s.is_empty() {
                None
            } else {
                Some(s)
            }
        }
        fn optional_num(s: u8) -> String {
            if s == 0 {
                String::default()
            } else {
                s.to_string()
            }
        }
        let secondary_addresses = secondary_addresses.join(" ");
        let secondary_addresses = optional_str(&secondary_addresses);
        let mail_server = optional_str(mail_server);
        let mail_port = optional_str(mail_port);
        let mail_user = optional_str(mail_user);
        let mail_password = optional_str(mail_password);
        let mail_security = optional_num(*mail_security as u8);
        let mail_security = optional_str(&mail_security);
        let send_server = optional_str(send_server);
        let send_port = optional_str(send_port);
        let send_user = optional_str(send_user);
        let send_password = optional_str(send_password);
        let send_security = optional_num(*send_security as u8);
        let send_security = optional_str(&send_security);

        let ctx = self.context();
        use deltachat::config::Config::*;
        ctx.set_config(Addr, Some(primary_address)).await?;
        ctx.set_config(SecondaryAddrs, secondary_addresses).await?;
        ctx.set_config(Displayname, Some(display_name)).await?;

        ctx.set_config(MailServer, mail_server).await?;
        ctx.set_config(MailPort, mail_port).await?;
        ctx.set_config(MailPw, mail_password).await?;
        ctx.set_config(MailUser, mail_user).await?;
        ctx.set_config(MailSecurity, mail_security).await?;
        ctx.set_config_bool(ImapCertificateChecks, mail_security.is_some())
            .await?;

        ctx.set_config(SendServer, send_server).await?;
        ctx.set_config(SendPort, send_port).await?;
        ctx.set_config(SendPw, send_password).await?;
        ctx.set_config(SendUser, send_user).await?;
        ctx.set_config(SendSecurity, send_security).await?;
        ctx.set_config_bool(ImapCertificateChecks, send_security.is_some())
            .await?;

        Ok(())
    }

    pub async fn login(&self, email: &str, password: &str) -> Result<()> {
        self.context()
            .set_config(Config::Addr, Some(email))
            .await
            .map_err(|err| anyhow!("Invalid email - {:?}", err))?;
        self.context()
            .set_config(Config::MailPw, Some(password))
            .await
            .map_err(|err| anyhow!("Invalid password - {:?}", err))?;

        self.configure().await?;
        Ok(())
    }

    pub async fn configure(&self) -> Result<()> {
        info!("configure");
        let is_configured = self.context().get_config_int(Config::Configured).await?;
        if is_configured == 1 {
            info!("Account already configured");
            return Ok(());
        } else {
            self.context()
                .configure()
                .await
                .map_err(|err| anyhow!("{:?}", err))?;
            Ok(())
        }
    }

    pub async fn load(&self, range: Option<(usize, usize)>) -> Result<Session> {
        if let Some((a, b)) = range {
            ensure!(a <= b, "invalid indicies");
        }
        let profile = self.load_info().await;
        let string_or_empty = move |cfg| async move {
            anyhow::Result::<String>::Ok(self.context().get_config(cfg).await?.unwrap_or_default())
        };
        let from_mail_sec = |sec: String| match sec.as_str() {
            "1" => NetSecurity::Strict,
            "2" => NetSecurity::Insecure2,
            "3" => NetSecurity::Insecure,
            _ => NetSecurity::Automatic,
        };
        let config = ProfileConfig {
            account_id: Some(profile.id),
            display_name: profile.display_name.as_ref().cloned().unwrap_or_default(),
            primary_address: profile.email.clone(),
            secondary_addresses: string_or_empty(Config::SecondaryAddrs)
                .await?
                .split(" ")
                .filter(|part| !part.is_empty())
                .map(|addr| addr.to_owned())
                .collect(),
            mail_server: string_or_empty(Config::MailServer).await?,
            mail_port: string_or_empty(Config::MailPort).await?,
            mail_user: string_or_empty(Config::MailUser).await?,
            mail_password: string_or_empty(Config::MailPw).await?,
            mail_security: from_mail_sec(string_or_empty(Config::MailSecurity).await?),
            send_server: string_or_empty(Config::SendServer).await?,
            send_port: string_or_empty(Config::SendPort).await?,
            send_user: string_or_empty(Config::SendUser).await?,
            send_password: string_or_empty(Config::SendPw).await?,
            send_security: from_mail_sec(string_or_empty(Config::SendSecurity).await?),
        };

        let chatlist = Chatlist::try_load(self.context(), 0, None, None)
            .await
            .map_err(|err| anyhow!("failed to load chats: {:?}", err))?;
        let total_len = chatlist.len();
        let range = range.unwrap_or((0, total_len));
        let stop_index = (range.1 + 1).min(total_len);
        let start_index = range.0;
        let len = stop_index.saturating_sub(start_index);

        let mut topics = Vec::with_capacity(len);
        for i in start_index..stop_index {
            let chat_id = chatlist.get_chat_id(i).with_context(|| format!("{}", i))?;
            let chat_state = ChatBackend::new(self.context().clone(), chat_id.to_u32())
                .load_info()
                .await
                .with_context(|| format!("chat id: {}", chat_id))?;

            topics.push(chat_state);
        }

        Ok(Session {
            profile,
            config,
            range: (start_index, stop_index.saturating_sub(1)),
            len: total_len,
            topics,
            selected_chat_id: self.get_selected_chat_id(),
        })
    }

    pub fn id(&self) -> u32 {
        self.context().get_id()
    }
    pub fn get_selected_chat_id(&self) -> Option<u32> {
        self.1
    }

    pub fn chat(&self, chat_id: u32) -> ChatBackend {
        ChatBackend::new(self.context().clone(), chat_id)
    }
}
