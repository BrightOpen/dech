use futures::{Future, Stream};
use std::{pin::Pin, task::Poll};

pub trait ReadyStreamExt {
    /// A future that completes immediately returning the stream poll status.
    ///
    /// Use it not to block on polling the stream if you are in a busy loop.
    ///
    /// Polling the future twice is an error as it always returns ready on first call.
    fn next_ready<'a>(&'a mut self) -> NextReady<&'a mut Self>;
}
impl<T> ReadyStreamExt for T
where
    T: Stream,
{
    fn next_ready<'a>(&'a mut self) -> NextReady<&'a mut Self> {
        NextReady::new(self)
    }
}

#[derive(Debug)]
pub struct NextReady<S> {
    pub inner: Option<Pin<Box<S>>>,
}

impl<S> NextReady<S> {
    pub fn new(inner: S) -> Self {
        Self {
            inner: Some(Box::pin(inner)),
        }
    }
}

impl<S> std::future::Future for NextReady<S>
where
    S: Stream,
{
    type Output = Option<Poll<S::Item>>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        let mut inner = self
            .inner
            .take()
            .expect("Please only poll this future once");
        Poll::Ready(match inner.as_mut().poll_next(cx) {
            Poll::Ready(None) => None,
            Poll::Ready(Some(item)) => Some(Poll::Ready(item)),
            Poll::Pending => Some(Poll::Pending),
        })
    }
}

pub trait ReadyFutureExt {
    /// A future that completes immediately returning the stream poll status.
    ///
    /// Use it not to block on polling the stream if you are in a busy loop.
    ///
    /// Polling the future twice is an error as it always returns ready on first call.
    fn ready<'a>(&'a mut self) -> FutureReady<&'a mut Self>;
}
impl<T> ReadyFutureExt for T
where
    T: Future,
{
    fn ready<'a>(&'a mut self) -> FutureReady<&'a mut Self> {
        FutureReady::new(self)
    }
}

#[derive(Debug)]
pub struct FutureReady<S> {
    pub inner: Option<Pin<Box<S>>>,
}

impl<S> FutureReady<S> {
    pub fn new(inner: S) -> Self {
        Self {
            inner: Some(Box::pin(inner)),
        }
    }
}

impl<S> std::future::Future for FutureReady<S>
where
    S: Future,
{
    type Output = Poll<S::Output>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        let mut inner = self
            .inner
            .take()
            .expect("Please only poll this future once");
        Poll::Ready(match inner.as_mut().poll(cx) {
            Poll::Ready(item) => Poll::Ready(item),
            Poll::Pending => Poll::Pending,
        })
    }
}
