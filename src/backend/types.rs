use chrono::{DateTime, Utc};
use num_derive::{FromPrimitive, ToPrimitive};
use std::{fmt::Display, path::PathBuf};

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Session {
    pub profile: Profile,
    pub config: ProfileConfig,
    pub range: (usize, usize),
    pub len: usize,
    pub topics: Vec<Topic>,
    pub selected_chat_id: Option<u32>,
}

#[derive(Debug, Default, PartialEq, Clone, Eq)]
pub struct Profile {
    pub id: u32,
    pub logged_in: Login,
    pub email: String,
    pub profile_image: Option<PathBuf>,
    pub display_name: Option<String>,
}

#[derive(Debug, Default, PartialEq, Clone, Eq)]
pub struct ProfileConfig {
    pub account_id: Option<u32>,
    /// Profile display name
    pub display_name: String,
    /// primary e-mail address
    pub primary_address: String,
    /// secondary e-mail addresses
    pub secondary_addresses: Vec<String>,

    pub mail_server: String,
    pub mail_port: String,
    pub mail_user: String,
    pub mail_password: String,
    pub mail_security: NetSecurity,

    pub send_server: String,
    pub send_port: String,
    pub send_user: String,
    pub send_password: String,
    pub send_security: NetSecurity,
}

#[derive(Debug, PartialEq, Clone, Copy, Eq)]
#[repr(u8)]
pub enum NetSecurity {
    // TODO: work this out
    Automatic = 0,
    Strict = 1,
    Insecure2 = 2,
    Insecure = 3,
}
impl Default for NetSecurity {
    fn default() -> Self {
        NetSecurity::Automatic
    }
}

#[derive(Debug, Default, PartialEq, Clone)]
pub struct Conversation {
    pub topic: Topic,
    pub range: (usize, usize),
    pub items: Vec<ChatItem>,
    pub messages: Vec<ChatMessage>,
    pub draft: MessageContent,
}

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Topic {
    pub index: Option<usize>,
    pub id: u32,
    pub name: String,
    pub header: String,
    pub preview: String,
    pub timestamp: DateTime<Utc>,
    pub state: String,
    pub profile_image: Option<PathBuf>,
    pub fresh_msg_cnt: usize,
    pub can_send: bool,
    pub is_contact_request: bool,
    pub is_self_talk: bool,
    pub is_device_talk: bool,
    pub chat_type: String,
    pub color: u32,
    pub member_count: usize,
    pub is_pinned: bool,
    pub is_archived: bool,
}

#[derive(Debug, PartialEq, Clone)]
pub enum ChatItem {
    Message(u32),
    DayMarker(DateTime<Utc>),
}

#[derive(Debug, PartialEq, Clone)]
pub enum ChatMessage {
    Message(MessageContent),
    DayMarker(DateTime<Utc>),
}

#[derive(Default, Debug, PartialEq, Clone)]
pub struct MessageContent {
    pub id: u32,
    pub from_id: u32,
    pub from_first_name: String,
    pub from_profile_image: Option<PathBuf>,
    pub from_color: u32,
    pub viewtype: Viewtype,
    pub state: String,
    pub text: Option<String>,
    pub quote: Option<Box<MessageContent>>,
    pub timestamp: DateTime<Utc>,
    pub is_info: bool,
    pub file: Option<PathBuf>,
    pub file_height: i32,
    pub file_width: i32,
    pub is_first: bool,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, FromPrimitive, ToPrimitive)]
#[repr(i32)]
pub enum Viewtype {
    Unknown = 0,
    Text = 10,
    Image = 20,
    Gif = 21,
    Sticker = 23,
    Audio = 40,
    Voice = 41,
    Video = 50,
    File = 60,
    VideochatInvitation = 70,
    Webxdc = 80,
}
impl Default for Viewtype {
    fn default() -> Self {
        Viewtype::Text
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Login {
    Success,
    Error(String),
    Progress(usize),
    Not,
}

impl Default for Login {
    fn default() -> Self {
        Login::Not
    }
}

impl Display for Login {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Login::Success => f.write_str("Online"),
            Login::Error(error) => write!(f, "Offline ({})", error),
            Login::Progress(progress) => write!(f, "Logging in ({})", progress),
            Login::Not => f.write_str("Offline"),
        }
    }
}
