use super::types::{MessageContent, Viewtype};
use anyhow::{anyhow, Result};
use chrono::{DateTime, NaiveDateTime, Utc};
use deltachat::{
    chat::ChatId,
    contact::{Contact, ContactId},
    context::Context,
    message::Message,
    message::MsgId,
};
use log::trace;
use num_traits::{FromPrimitive, ToPrimitive};
use std::collections::HashMap;

#[derive(Debug)]
pub struct MessageBackend(Context, ChatId, MsgId);

impl MessageBackend {
    pub fn new(context: Context, chat_id: u32, message_id: u32) -> Self {
        Self(context, ChatId::new(chat_id), MsgId::new(message_id))
    }

    pub fn id(&self) -> u32 {
        self.message().to_u32()
    }
    pub fn account_id(&self) -> u32 {
        self.context().get_id()
    }
    pub fn chat_id(&self) -> u32 {
        self.chat().to_u32()
    }

    fn context(&self) -> &Context {
        &self.0
    }
    fn chat(&self) -> ChatId {
        self.1
    }
    fn message(&self) -> MsgId {
        self.2
    }

    pub async fn load_message(
        context: &Context,
        contacts: &mut HashMap<ContactId, Contact>,
        msg: &Message,
    ) -> Result<MessageContent> {
        let mut our = Self::load_message_no_quote(context, contacts, msg).await?;

        if let Some(quote) = msg.quoted_message(&context).await? {
            our.quote = Some(Box::new(
                Self::load_message_no_quote(context, contacts, &quote).await?,
            ));
        }

        Ok(our)
    }

    pub async fn load_message_no_quote(
        context: &Context,
        contacts: &mut HashMap<ContactId, Contact>,
        msg: &Message,
    ) -> Result<MessageContent> {
        let mut our = MessageContent {
            id: msg.get_id().to_u32(),
            from_id: msg.get_from_id().to_u32(),
            viewtype: Viewtype::from_i32(msg.get_viewtype().to_i32().unwrap()).unwrap(),
            from_first_name: Default::default(),
            from_profile_image: Default::default(),
            from_color: Default::default(),
            state: msg.get_state().to_string(),
            text: msg.get_text(),
            quote: None,
            timestamp: Self::get_timestamp(msg.get_sort_timestamp()),
            is_info: msg.is_info(),
            file: msg.get_file(&context).map(Into::into),
            file_width: msg.get_width(),
            file_height: msg.get_height(),
            is_first: true,
        };

        let from_id = msg.get_from_id();
        if !from_id.is_special() {
            let from = match contacts.get(&from_id) {
                Some(contact) => contact,
                None => contacts.entry(msg.get_from_id()).or_insert(
                    Contact::load_from_db(&context, msg.get_from_id())
                        .await
                        .map_err(|err| {
                            anyhow!("failed to load contact: {}: {}", msg.get_from_id(), err)
                        })?,
                ),
            };
            our.from_first_name = from.get_display_name().to_string();
            our.from_profile_image = from.get_profile_image(context).await?.map(Into::into);
            our.from_color = from.get_color();
        }

        Ok(our)
    }

    pub fn get_timestamp(ts: i64) -> DateTime<Utc> {
        trace!("get_timestamp {}", ts);
        let naive = NaiveDateTime::from_timestamp(ts, 0);
        DateTime::from_utc(naive, Utc)
    }
}
