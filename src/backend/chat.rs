use super::{
    draft::DraftBackend,
    message::MessageBackend,
    types::{ChatItem, ChatMessage, Conversation, Topic, Viewtype},
};
use anyhow::{anyhow, Result};
use deltachat::{
    chat::{self, Chat as DeltaChatChat, ChatId, ChatVisibility},
    chatlist::Chatlist,
    context::Context,
    message::{self, Message, MsgId},
};
use log::{info, trace};
use num_traits::{FromPrimitive, ToPrimitive};
use std::collections::HashMap;
#[derive(Debug, Clone)]
pub struct ChatBackend(Context, ChatId);

impl ChatBackend {
    pub fn new(context: Context, chat_id: u32) -> Self {
        Self(context, ChatId::new(chat_id))
    }

    pub fn id(&self) -> u32 {
        self.chat().to_u32()
    }
    pub fn account_id(&self) -> u32 {
        self.context().get_id()
    }

    fn context(&self) -> &Context {
        &self.0
    }
    fn chat(&self) -> ChatId {
        self.1
    }

    pub async fn load(&self, range: Option<(usize, usize)>) -> Result<Conversation> {
        let (range, items, messages) = self.load_messages(range).await?;
        let topic = self.load_info().await?;
        let draft = self.draft().await?.load().await?;
        Ok(Conversation {
            topic,
            range,
            items,
            messages,
            draft,
        })
    }

    pub async fn load_info(&self) -> Result<Topic> {
        info!("selecting chat {:?}", self.chat());
        let chatlist = Chatlist::try_load(self.context(), 0, None, None)
            .await
            .map_err(|err| anyhow!("failed to load chats: {:?}", err))?;
        let selected_chat = load_chat_info(self.context(), self.chat(), &chatlist).await?;

        // mark as noticed
        chat::marknoticed_chat(self.context(), self.chat())
            .await
            .map_err(|err| anyhow!("failed to mark noticed: {:?}", err))?;

        Ok(selected_chat)
    }

    pub async fn load_messages(
        &self,
        range: Option<(usize, usize)>,
    ) -> Result<((usize, usize), Vec<ChatItem>, Vec<ChatMessage>)> {
        info!("loading {:?} msgs", self.chat());

        let (range, chat_items, chat_messages) =
            refresh_message_list(self.context(), self.chat(), range).await?;

        let msg_ids: Vec<_> = chat_messages
            .iter()
            .filter_map(|item| match item {
                ChatMessage::Message(inner) => Some(message::MsgId::new(inner.id)),
                ChatMessage::DayMarker(..) => None,
            })
            .collect();

        {
            message::markseen_msgs(self.context(), msg_ids).await?;
        }

        Ok((range, chat_items, chat_messages))
    }

    pub async fn draft(&self) -> Result<DraftBackend> {
        trace!(
            "Loading draft for {}/{}",
            self.context().get_id(),
            self.chat().to_u32()
        );
        if let Some(draft) = self.chat().get_draft(self.context()).await? {
            Ok(DraftBackend::new(
                self.context().clone(),
                self.chat(),
                draft,
            ))
        } else {
            trace!(
                "Creating draft for {}/{}",
                self.context().get_id(),
                self.chat().to_u32()
            );
            let mut msg = Message::new(message::Viewtype::Text);
            msg.set_text(Some(String::default()));
            Ok(DraftBackend::new(self.context().clone(), self.chat(), msg))
        }
    }

    pub async fn send_text_message(&self, text: String) -> Result<()> {
        chat::send_text_msg(self.context(), self.chat(), text)
            .await
            .map_err(|err| anyhow!("failed to send message: {}", err))?;

        Ok(())
    }

    pub async fn send_file_message(
        &self,
        typ: Viewtype,
        path: String,
        text: Option<String>,
        mime: Option<String>,
    ) -> Result<MessageBackend> {
        let mut msg = message::Message::new(
            deltachat::message::Viewtype::from_i32(typ.to_i32().unwrap()).unwrap(),
        );
        msg.set_text(text);
        msg.set_file(path, mime.as_deref());

        chat::send_msg(self.context(), self.chat(), &mut msg)
            .await
            .map(|m| MessageBackend::new(self.context().clone(), self.chat().to_u32(), m.to_u32()))
            .map_err(|err| anyhow!("failed to send message: {}", err))
    }

    pub async fn accept_contact_request(&self) -> Result<()> {
        self.chat().accept(self.context()).await
    }

    pub async fn block_contact(&self) -> Result<()> {
        self.chat().block(self.context()).await
    }

    pub async fn pin(&self) -> Result<()> {
        info!("pinning chat: {:?}", self.chat());
        self.chat()
            .set_visibility(self.context(), ChatVisibility::Pinned)
            .await
    }

    pub async fn unpin(&self) -> Result<()> {
        info!("unpinning chat: {:?}", self.chat());
        self.chat()
            .set_visibility(self.context(), ChatVisibility::Normal)
            .await
    }

    pub async fn archive(&self) -> Result<()> {
        info!("archiving chat: {:?}", self.chat());
        self.chat()
            .set_visibility(self.context(), ChatVisibility::Archived)
            .await
    }

    pub async fn unarchive(&self) -> Result<()> {
        info!("unarchiving chat: {:?}", self.chat());
        self.chat()
            .set_visibility(self.context(), ChatVisibility::Normal)
            .await
    }
}

async fn load_chat_info(context: &Context, cid: ChatId, chats: &Chatlist) -> Result<Topic> {
    let chat = DeltaChatChat::load_from_db(&context, cid).await?;
    if let Some(index) = chats.get_index_for_id(cid) {
        let lot = chats.get_summary(&context, index, Some(&chat)).await?;

        let header = lot
            .prefix
            .as_ref()
            .map(|s| s.to_string())
            .unwrap_or_default();
        let preview = lot.truncated_text(160).to_string();

        let index = chats.get_index_for_id(cid);
        let is_contact_request = chat.is_contact_request();

        Ok(Topic {
            id: cid.to_u32(),
            index,
            name: chat.get_name().to_string(),
            header,
            preview,
            timestamp: MessageBackend::get_timestamp(lot.timestamp),
            state: lot.state.to_string(),
            profile_image: chat.get_profile_image(&context).await?.map(Into::into),
            can_send: chat.can_send(&context).await.unwrap_or_default(),
            is_contact_request,
            chat_type: chat.get_type().to_string(),
            color: chat.get_color(&context).await?,
            is_device_talk: chat.is_device_talk(),
            is_self_talk: chat.is_self_talk(),
            fresh_msg_cnt: cid.get_fresh_msg_cnt(&context).await?,
            member_count: deltachat::chat::get_chat_contacts(&context, cid)
                .await?
                .len(),
            is_pinned: chat.get_visibility() == ChatVisibility::Pinned,
            is_archived: chat.get_visibility() == ChatVisibility::Archived,
        })
    } else {
        anyhow::bail!(
            "chat {} not found in the list for {}",
            cid,
            context.get_id()
        );
    }
}

async fn refresh_message_list(
    context: &Context,
    chat_id: ChatId,
    range: Option<(usize, usize)>,
) -> Result<((usize, usize), Vec<ChatItem>, Vec<ChatMessage>)> {
    let chat_items: Vec<_> =
        chat::get_chat_msgs(&context, chat_id, deltachat::constants::DC_GCM_ADDDAYMARKER)
            .await?
            .into_iter()
            .map(|item| match item {
                chat::ChatItem::Message { msg_id } => ChatItem::Message(msg_id.to_u32()),
                chat::ChatItem::DayMarker { timestamp } => {
                    ChatItem::DayMarker(MessageBackend::get_timestamp(timestamp))
                }
            })
            .collect();

    let total_len = chat_items.len();

    // default to the last n items
    let range = range.unwrap_or_else(|| (total_len.saturating_sub(50), total_len));

    info!(
        "loading chat messages {}/{} from ({}..={})",
        context.get_id(),
        chat_id.to_u32(),
        range.0,
        range.1
    );

    let len = range.1.saturating_sub(range.0);
    let offset = range.0;

    let mut chat_messages = Vec::with_capacity(len);
    let mut contacts = HashMap::new();

    let mut last_contact_id = None;
    let mut last_marker = true;
    for chat_item in chat_items.iter().skip(offset).take(len) {
        match chat_item {
            ChatItem::Message(msg_id) => {
                let msg = message::Message::load_from_db(&context, MsgId::new(*msg_id))
                    .await
                    .map_err(|err| {
                        anyhow!(
                            "failed to load msg: {}/{}/{}: {}",
                            context.get_id(),
                            chat_id.to_u32(),
                            msg_id,
                            err
                        )
                    })?;

                let is_first = if last_marker {
                    true
                } else {
                    if let Some(id) = last_contact_id {
                        id != msg.get_from_id()
                    } else {
                        true
                    }
                };
                last_contact_id = Some(msg.get_from_id());
                last_marker = false;

                let mut inner_msg =
                    MessageBackend::load_message(context, &mut contacts, &msg).await?;
                inner_msg.is_first = is_first;

                chat_messages.push(ChatMessage::Message(inner_msg));
            }
            ChatItem::DayMarker(t) => {
                chat_messages.push(ChatMessage::DayMarker(*t));
                last_marker = true;
            }
        }
    }

    Ok((range, chat_items, chat_messages))
}
