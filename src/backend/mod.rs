pub mod account;
pub mod chat;
pub mod draft;
pub mod message;
pub mod types;
pub mod util;

use self::account::AccountBackend;
use self::chat::ChatBackend;
use anyhow::{anyhow, Result};
use deltachat::EventEmitter;
use log::info;
use std::collections::HashMap;
use std::path::PathBuf;

#[derive(Debug)]
pub struct Backend {
    accounts: deltachat::accounts::Accounts,
    selected_chats: HashMap<u32, u32>,
    dir: PathBuf,
}

#[cfg(not(target_os = "linux"))]
lazy_static::lazy_static! {
    pub static ref HOME_DIR: PathBuf = dirs::home_dir()
        .unwrap_or_else(|| "home".into())
        .join(".deltachat")
        .into();
}

#[cfg(target_os = "linux")]
lazy_static::lazy_static! {
    pub static ref HOME_DIR: PathBuf = dirs::home_dir()
        .unwrap_or_else(|| "home".into())
        .join(".config/DeltaChat/accounts")
        .into();
}

impl Backend {
    /// Open accounts from the given or default dir
    pub async fn start(dir: impl Into<Option<PathBuf>>) -> Result<Self> {
        info!("restoring local state");
        let dir = dir.into().unwrap_or_else(|| HOME_DIR.clone());
        let accounts = deltachat::accounts::Accounts::new(dir.clone()).await?;

        let me = Self {
            dir,
            accounts,
            selected_chats: Default::default(),
        };

        info!("loaded backend");

        me.accounts.start_io().await;

        info!("started io");

        Ok(me)
    }

    pub async fn maybe_network(&self) -> Result<()> {
        self.accounts.maybe_network().await;
        Ok(())
    }

    pub fn get_all_accounts(&self) -> Result<Vec<AccountBackend>> {
        let ids = self.accounts.get_all();
        let mut list = vec![];
        for id in ids {
            list.push(self.account(id)?);
        }
        Ok(list)
    }
    pub fn account(&self, account_id: u32) -> Result<AccountBackend> {
        if let Some(context) = self.accounts.get_account(account_id) {
            let scid = self.get_selected_chat_id(&context.get_id());
            Ok(AccountBackend::new(context, scid))
        } else {
            Err(anyhow!("invalid account: {}", account_id))
        }
    }
    pub async fn add_account(&mut self) -> Result<AccountBackend> {
        let account_id = self.accounts.add_account().await?;
        self.account(account_id)
    }
    pub async fn select_account(&mut self, account_id: u32) -> Result<AccountBackend> {
        self.accounts.select_account(account_id).await?;
        self.account(account_id)
    }
    pub async fn select_chat(&mut self, account_id: u32, chat_id: u32) -> Result<ChatBackend> {
        self.selected_chats.insert(account_id, chat_id);
        Ok(self.account(account_id)?.chat(chat_id))
    }
    pub async fn remove_account(&mut self, account_id: u32) -> Result<()> {
        self.accounts.remove_account(account_id).await
    }

    pub fn get_selected_account(&self) -> Option<AccountBackend> {
        if let Some(context) = self.accounts.get_selected_account() {
            let scid = self.get_selected_chat_id(&context.get_id());
            Some(AccountBackend::new(context, scid))
        } else {
            None
        }
    }
    pub fn get_selected_account_id(&self) -> Option<u32> {
        self.accounts.get_selected_account_id()
    }
    fn get_selected_chat_id(&self, account_id: &u32) -> Option<u32> {
        self.selected_chats.get(&account_id).cloned()
    }

    pub fn get_event_emitter(&self) -> EventEmitter {
        self.accounts.get_event_emitter()
    }
}

impl PartialEq for Backend {
    fn eq(&self, other: &Self) -> bool {
        self.dir == other.dir
    }
}