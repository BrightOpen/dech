use std::collections::HashMap;

use super::{message::MessageBackend, types::MessageContent};
use anyhow::Result;
use deltachat::{chat::ChatId, context::Context, message::Message};
use log::trace;

#[derive(Debug)]
pub struct DraftBackend(Context, ChatId, Message);

impl DraftBackend {
    pub fn new(context: Context, chat_id: ChatId, message: Message) -> Self {
        Self(context, chat_id, message)
    }

    pub fn id(&self) -> u32 {
        self.message().get_id().to_u32()
    }
    pub fn account_id(&self) -> u32 {
        self.context().get_id()
    }
    pub fn chat_id(&self) -> u32 {
        self.message().get_chat_id().to_u32()
    }

    fn context(&self) -> &Context {
        &self.0
    }
    fn chat(&self) -> ChatId {
        self.1
    }
    fn message(&self) -> &Message {
        &self.2
    }

    pub async fn load(&self) -> Result<MessageContent> {
        MessageBackend::load_message(self.context(), &mut HashMap::default(), self.message()).await
    }
    pub async fn save(&mut self, content: &mut MessageContent) -> Result<()> {
        trace!("Saving draft on {}/{}", self.account_id(), self.chat_id());
        let mut msg = self
            .chat()
            .get_draft(self.context())
            .await?
            .unwrap_or_else(|| Message::new(deltachat::message::Viewtype::Text));
        msg.set_text(content.text.clone());
        self.chat()
            .set_draft(self.context(), Some(&mut msg))
            .await?;
        self.2 = msg;
        *content = self.load().await?;
        Ok(())
    }
    pub async fn delete(&mut self, content: &mut MessageContent) -> Result<()> {
        self.chat().set_draft(self.context(), None).await?;
        self.2 = Message::new(deltachat::message::Viewtype::Text);
        *content = self.load().await?;
        Ok(())
    }
    pub async fn send(&mut self, content: &mut MessageContent) -> Result<()> {
        self.save(content).await?;
        trace!("Sending draft on {}/{}", self.account_id(), self.chat_id());
        deltachat::chat::send_msg(&self.0, self.1, &mut self.2).await?;
        self.delete(content).await
    }
}
