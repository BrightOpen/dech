#[tokio::main]
async fn main() -> anyhow::Result<()> {
    env_logger::init();
    dech::app::Dech::default().run_tui().await
}
