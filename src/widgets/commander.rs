use super::{
    make_ui_main_view_account_action_preview, make_ui_main_view_account_preview,
    make_ui_main_view_conversation, make_ui_main_view_conversation_action_preview,
    make_ui_main_view_conversation_preview, make_ui_main_view_welcome, Action, Configurator,
};
use crate::{
    app::{
        command::{DeleteAccount, SelectAccount, SelectChat, UnselectAccount, UnselectChat},
        Dech,
    },
    backend::types::Login,
};
use log::trace;
use termit::{
    geometry::*,
    prelude::*,
    widget::{AnchorPlaced, Canvas, Stack, TextBox, Widget, WidgetExt},
    Painter, Screen,
};

#[derive(Default)]
pub struct Commander {
    active_action_description: String,
    active_action_index: usize,
    prefer_action_index: Option<usize>,
    actions: Vec<Action<Dech>>,
    generation: usize,
}

impl<A: AppEvent> Widget<Dech, A> for Commander {
    fn update(
        &mut self,
        model: &mut Dech,
        input: &Event<A>,
        screen: &mut Screen,
        painter: &Painter,
    ) -> Window {
        let mut select = match self.active_action_description.is_empty() {
            true => Some(0isize),
            false => None,
        };
        let mut activate = false;
        let action_count = self.actions.len();
        match input {
            Event::Mouse(mouse) => match mouse.kind {
                MouseEventKind::ScrollDown if model.show_menu && action_count != 0 => {
                    select = Some(1)
                }
                MouseEventKind::ScrollUp if model.show_menu && action_count != 0 => {
                    select = Some(-1)
                }
                MouseEventKind::Up(btn)
                    if btn == MouseButton::Left && mouse.modifiers.is_empty() =>
                {
                    if let Some((i, action)) = self
                        .actions
                        .iter()
                        .enumerate()
                        .filter(|(_i, a)| !(a.hidden)(a, model))
                        .filter(|(_i, a)| (a.area & point(mouse.column, mouse.row)).is_some())
                        .next()
                    {
                        // left-clicked on an action
                        // => activate action
                        self.active_action_description = action.description.clone();
                        self.active_action_index = i;
                        activate = true;
                    }
                }
                MouseEventKind::Up(btn)
                    if btn == MouseButton::Left && mouse.modifiers.contains(&KeyModifier::Alt) =>
                {
                    if let Some((i, action)) = self
                        .actions
                        .iter()
                        .enumerate()
                        .filter(|(_i, a)| !(a.hidden)(a, model))
                        .filter(|(_i, a)| (a.area & point(mouse.column, mouse.row)).is_some())
                        .next()
                    {
                        // left-clicked on an action with ALT
                        // => select action
                        self.active_action_description = action.description.clone();
                        self.active_action_index = i;
                        select = Some(0);
                    }
                }
                MouseEventKind::Moved => {
                    // ignore mouse moves
                }
                _ if model.show_menu => select = Some(0),
                _ => {}
            },
            Event::Key(key) => match key.keycode {
                KeyCode::Esc => {
                    model.search.clear();
                    model.main_preview = None;
                    model.show_menu = !model.show_menu;
                }
                KeyCode::Char('q') if key.modifiers.contains(&KeyModifier::Control) => {
                    model.is_terminating = true;
                }
                KeyCode::Enter if model.show_menu && action_count != 0 => {
                    activate = true;
                }
                KeyCode::Up if model.show_menu && action_count != 0 => {
                    select = Some(-1);
                }
                KeyCode::Down if model.show_menu && action_count != 0 => {
                    select = Some(1);
                }
                KeyCode::PageUp if model.show_menu && action_count != 0 => {
                    let page_size = self.actions.iter().filter(|a| a.area.height != 0).count();
                    let max_ahead = self
                        .actions
                        .iter()
                        .take(self.active_action_index)
                        .filter(|a| !(a.hidden)(a, model))
                        .count();
                    let jump = page_size.clamp(0, max_ahead);
                    select = Some(-(jump as isize));
                }
                KeyCode::PageDown if model.show_menu && action_count != 0 => {
                    let page_size = self.actions.iter().filter(|a| a.area.height != 0).count();
                    let max_behind = self
                        .actions
                        .iter()
                        .skip(self.active_action_index + 1)
                        .filter(|a| !(a.hidden)(a, model))
                        .count();
                    let jump = page_size.clamp(0, max_behind);
                    select = Some(jump as isize);
                }

                _ if model.show_menu => select = Some(0),
                _ => {}
            },
            _ => {}
        }
        if let Some(ref mut shift) = select {
            if !self.actions.is_empty() {
                let mut idx = self.active_action_index.clamp(0, action_count);
                trace!("Shift {} by {}", idx, shift);
                while *shift != 0 {
                    let fix = if shift.is_negative() {
                        idx = (idx + action_count - 1) % action_count;
                        1
                    } else {
                        idx = (idx + 1) % action_count;
                        -1
                    };
                    let action = &self.actions[idx];
                    if !(action.hidden)(&action, model) {
                        *shift += fix;
                    }
                }
                self.active_action_index = idx;
                self.active_action_description = self.actions[idx].description.clone();
            }
        }

        if activate {
            model.search.clear();
            model.main_preview = None;
            if let Some(a) = self.actions.get_mut(self.active_action_index) {
                a.action(model);
            }
        }

        if model.generation != self.generation {
            self.reload_actions::<A>(model);
            // dry-run ui pass to pre-load action sizes
            self.render_ui(model, input, screen, &painter);
        }

        self.update_action_selection(model);

        if select.is_some() {
            if let Some(a) = self.actions.get_mut(self.active_action_index) {
                model.main_preview = None;
                a.select(model);
            }
        }

        self.generation = model.generation;

        self.render_ui(model, input, screen, &painter)
    }
}

impl Commander {
    fn render_ui<A: AppEvent>(
        &mut self,
        model: &mut Dech,
        input: &Event<A>,
        screen: &mut Screen,
        painter: &Painter,
    ) -> Window {
        let len = model.search.len();
        let fg = Color::blue(false);
        let bg = Color::rgb(
            len.clamp(0, 250) as u8,
            (len.clamp(250, 500) - 250) as u8,
            (len.clamp(500, 750) - 500) as u8,
        );

        let start = {
            // calculate first action to display
            // so that active action shows
            let mut start = self.active_action_index;
            let mut space_left = painter.scope().height.saturating_sub(5);
            for height in self
                .actions
                .iter()
                .take(self.active_action_index + 1)
                .filter(|a| !(a.hidden)(a, model))
                .map(|a| a.area.height.clamp(1, u16::MAX))
                .rev()
            {
                space_left = space_left.saturating_sub(height);
                if space_left == 0 {
                    break;
                } else {
                    start = start.saturating_sub(1);
                }
            }
            start.clamp(0, self.actions.len())
        };

        Canvas::with_background(
            Color::rgb(20, 20, 20),
            Stack::south_east_boxes(vec![])
                .add_box({
                    let mut textbox = TextBox::new(|m: &mut Dech| Some(&mut m.search));
                    textbox.active = model.show_menu;
                    Canvas::new(
                        Stack::east_south_boxes(vec![])
                            .add_box("> ")
                            .add_box(textbox),
                    )
                    .front(fg)
                    .back(bg)
                    .place()
                    .top(0)
                    .height(1)
                })
                .add_box(Stack::south_east(&mut self.actions[start..]).place().top(1))
                .place()
                .top(1)
                .left(2)
                .right(2)
                .bottom(1),
        )
        .update(model, input, screen, &painter)
    }

    fn update_action_selection(&mut self, model: &Dech) {
        let mut out_of_sync = self.active_action_index >= self.actions.len()
            || self.actions[self.active_action_index].description != self.active_action_description
            || (self.actions[self.active_action_index].hidden)(
                &self.actions[self.active_action_index],
                model,
            );

        for (i, action) in self.actions.iter_mut().enumerate() {
            if out_of_sync
                && (self.prefer_action_index == Some(i)
                    || action.description == self.active_action_description)
                && !(action.hidden)(action, model)
            {
                out_of_sync = false;
                self.active_action_index = i;
                self.active_action_description = action.description.clone();
            }

            action.selected = i == self.active_action_index
        }

        if out_of_sync {
            if let Some((i, action)) = self
                .actions
                .iter_mut()
                .enumerate()
                .filter(|(_i, a)| !(a.hidden)(a, model))
                .next()
            {
                //out_of_sync = false;
                self.active_action_index = i;
                self.active_action_description = action.description.clone();
                action.selected = true;
            }
        }
    }
    fn reload_actions<A: AppEvent>(&mut self, model: &Dech) {
        self.actions.clear();
        self.reload_chat_actions::<A>(model);
        self.reload_account_actions::<A>(model);
        self.reload_common_actions(model);
    }
    fn reload_common_actions(&mut self, _model: &Dech) {
        let style = Style::default();
        let danger_style = style.front(Color::red(false));
        let help_style = style.front(Color::magenta(false));

        let filtered_out = |a: &Action<Dech>, m: &Dech| {
            !a.description
                .to_lowercase()
                .contains(&m.search.to_lowercase())
        };

        self.actions.extend(vec![
            Action::new("help and about", help_style)
                .hide_when(filtered_out)
                .on_select(|m| m.main_preview = Some(Box::new(make_ui_main_view_welcome())))
                .on_action(|m| m.main_view = Box::new(make_ui_main_view_welcome())),
            Action::new("quit (Ctrl+q)", danger_style)
                .hide_when(filtered_out)
                .on_select(|m| {
                    m.main_preview = Some(Box::new("Close Dech, the text based delta.chat client."))
                })
                .on_action(|m: &mut Dech| m.is_terminating = true),
            Action::new("cancel (Esc)", danger_style)
                .hide_when(|_, m: &Dech| m.search.is_empty())
                .on_select(|m| {
                    m.main_preview = Some(Box::new("Cancel the current search and hide the menu."))
                })
                .on_action(|m| m.show_menu = false),
        ]);
    }

    fn reload_account_actions<A: AppEvent>(&mut self, model: &Dech) {
        let style = Style::default();
        let online_style = style.front(Color::green(false));
        let offline_style = style.front(Color::yellow(false));
        let acc_style = style.front(Color::blue(false));

        let filtered_out = |a: &Action<Dech>, m: &Dech| {
            !a.description
                .to_lowercase()
                .contains(&m.search.to_lowercase())
                || m.session.is_some()
        };

        for info in model.profiles.iter() {
            let account_id = info.id;
            let name = format!(
                "{}",
                info.display_name.as_ref().unwrap_or_else(|| &info.email),
            );
            self.actions.push(
                Action::new(
                    name,
                    if info.logged_in == Login::Success {
                        online_style
                    } else {
                        offline_style
                    },
                )
                .on_select(move |m: &mut Dech| {
                    m.main_preview = Some(Box::new(make_ui_main_view_account_preview(
                        account_id,
                        "Enter this profile",
                    )))
                })
                .on_action(move |m: &mut Dech| {
                    m.send_command(SelectAccount { account_id });
                })
                .hide_when(filtered_out),
            )
        }

        self.actions.extend(
            vec![Action::new("add a new profile", acc_style)
                .hide_when(filtered_out)
                .on_select(|m| m.main_preview = Some(Box::new("Add a new profile")))
                .on_action(|m| {
                    m.new_config = Default::default();
                    m.main_view = Box::new(Configurator::default());
                    m.show_menu = false
                })]
            .into_iter(),
        );
    }
    fn reload_chat_actions<A: AppEvent>(&mut self, model: &Dech) {
        let style = Style::default().front(Color::grey(false));
        let chat_style = style.front(Color::cyan(false));
        let acc_style = style.front(Color::blue(false));
        let danger_style = style.front(Color::red(false));

        let filtered_out = |a: &Action<Dech>, m: &Dech| {
            !a.description
                .to_lowercase()
                .contains(&m.search.to_lowercase())
                || m.session.is_none()
        };
        let no_chat = |_a: &Action<Dech>, m: &Dech| m.conversation.is_none();

        self.actions.extend(vec![
            Action::new("close this chat", style)
                .hide_when(filtered_out)
                .hide_when(no_chat)
                .on_select(|m| {
                    m.main_preview = Some(Box::new(make_ui_main_view_conversation_action_preview(
                        "Close the chat",
                    )))
                })
                .on_action(|m| m.send_command(UnselectChat)),
            Action::new("start a new chat (todo)", style)
                .hide_when(filtered_out)
                .on_select(|m| m.main_preview = Some(Box::new("Start a new chat"))),
            Action::new("manage chat peers (todo)", style)
                .hide_when(filtered_out)
                .hide_when(no_chat)
                .on_select(|m| {
                    m.main_preview = Some(Box::new(make_ui_main_view_conversation_action_preview(
                        "Add or remove chat peers",
                    )))
                }),
            Action::new("show chat galery (todo)", style)
                .hide_when(filtered_out)
                .hide_when(no_chat)
                .on_select(|m| {
                    m.main_preview = Some(Box::new(make_ui_main_view_conversation_action_preview(
                        "Explore media and files mentioned this conversation",
                    )))
                }),
            Action::new("remove this chat (todo)", danger_style)
                .hide_when(filtered_out)
                .hide_when(no_chat)
                .on_select(|m| {
                    m.main_preview = Some(Box::new(make_ui_main_view_conversation_action_preview(
                        "Remove this chat and conversation history",
                    )))
                }),
        ]);

        if let Some(ref state) = model.session {
            for chat in state.topics.iter() {
                let name = format!("{}", chat.name);
                let chat_id = chat.id;
                let account_id = state.profile.id;
                if Some(chat_id) == state.selected_chat_id {
                    self.prefer_action_index = Some(self.actions.len());
                }
                self.actions.push(
                    Action::new(name, chat_style)
                        .on_select(move |m: &mut Dech| {
                            m.main_preview =
                                Some(Box::new(make_ui_main_view_conversation_preview(
                                    chat_id,
                                    "Open the conversation",
                                )));
                        })
                        .on_action(move |m| {
                            m.main_view = Box::new(make_ui_main_view_conversation());
                            m.send_command(SelectChat {
                                account_id,
                                chat_id,
                            });
                            m.show_menu = false
                        })
                        .hide_when(filtered_out),
                )
            }
        }

        self.actions.extend(vec![
            Action::new("switch profile", acc_style)
                .hide_when(filtered_out)
                .on_select(move |m: &mut Dech| {
                    m.main_preview = Some(Box::new(make_ui_main_view_account_action_preview(
                        "Close this profile so you can open another one",
                    )))
                })
                .on_action(move |m| {
                    m.send_command(UnselectAccount);
                    m.main_view = Box::new(make_ui_main_view_welcome())
                }),
            Action::new("configure this profile", acc_style)
                .hide_when(filtered_out)
                .on_select(move |m: &mut Dech| {
                    m.main_preview = Some(Box::new(make_ui_main_view_account_action_preview(
                        "Change profile configuration",
                    )))
                })
                .on_action(move |m| {
                    m.main_view = Box::new(Configurator::default());
                    m.show_menu = false;
                }),
            Action::new("backup this profile (todo)", acc_style)
                .hide_when(filtered_out)
                .on_select(move |m: &mut Dech| {
                    m.main_preview = Some(Box::new(make_ui_main_view_account_action_preview(
                        "Save a backup of this profile and converstaions",
                    )))
                }),
            Action::new("delete this profile", danger_style)
                .hide_when(filtered_out)
                .on_select(move |m: &mut Dech| {
                    m.main_preview = Some(Box::new(make_ui_main_view_account_action_preview(
                        "!!! Delete this profile and all it's conversations and files !!!"
                            .front(Color::black())
                            .back(Color::red(false)),
                    )))
                })
                .on_action(move |m: &mut Dech| {
                    m.main_view = Box::new(make_ui_main_view_welcome());
                    if let Some(session) = &m.session {
                        m.send_command(DeleteAccount {
                            account_id: session.profile.id,
                        })
                    }
                }),
        ]);
    }
}
