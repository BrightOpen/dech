use std::{
    collections::VecDeque,
    time::{Duration, Instant},
};

use crate::app::Dech;
use termit::prelude::*;

/// Reduce cycling if not in use
pub struct Nice {
    durations: VecDeque<Duration>,
    last: Instant,
    show: bool,
    focused: bool,
}
impl Default for Nice {
    fn default() -> Self {
        Self {
            durations: vec![Duration::ZERO; 600].into_iter().collect(),
            last: Instant::now(),
            show: true,
            focused: true,
        }
    }
}
impl<A: AppEvent> Widget<Dech, A> for Nice {
    fn update(
        &mut self,
        model: &mut Dech,
        input: &Event<A>,
        screen: &mut Screen,
        painter: &Painter,
    ) -> Window {
        self.durations.push_back(self.last.elapsed());
        self.durations.pop_front();
        self.last = Instant::now();

        if let Event::Focus(focused) = input {
            self.focused = *focused
        }

        if self.show && matches!(input, Event::Refresh(_)) {
            // show stats over longer period
            let (count, total_duration) =
                self.durations.iter().rev().fold((0, 0.0), |(c, t), a| {
                    if t < 3.0 {
                        (c + 1, t + a.as_secs_f64())
                    } else {
                        (c, t)
                    }
                });
            let fps = count as f64 / total_duration;
            format!("FPS:{fps:0.5}").update(model, input, screen, painter)
        } else {
            // no space taken
            window(painter.scope().position(), point(0, 0))
        }
    }
}
