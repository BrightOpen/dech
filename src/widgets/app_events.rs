use crate::app::{command::HandleEvent, Dech, DechEvent};

use termit::prelude::*;

/// Reduce cycling if not in use
#[derive(Default)]
pub struct AppEvents;
impl Widget<Dech, DechEvent> for AppEvents {
    fn update(
        &mut self,
        model: &mut Dech,
        input: &Event<DechEvent>,
        _screen: &mut Screen,
        painter: &Painter,
    ) -> Window {
        match input {
            Event::App(appev) => match appev {
                DechEvent::NoOp => {}
                DechEvent::Backend(dce) => model.send_command(HandleEvent {
                    account_id: dce.id,
                    kind: dce.typ.clone(),
                }),
            },
            _ => {}
        }
        // no space taken
        window(painter.scope().position(), point(0, 0))
    }
}
