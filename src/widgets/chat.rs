use std::borrow::Cow;

use crate::app::Dech;
use deltachat::contact::ContactId;
use termit::prelude::*;

#[derive(Default)]
pub struct Chat {}

impl<A: AppEvent> Widget<Dech, A> for Chat {
    fn update(
        &mut self,
        model: &mut Dech,
        input: &Event<A>,
        screen: &mut termit::Screen,
        painter: &termit::Painter,
    ) -> termit::geometry::Window {
        let mut scroll_screen = Screen::new(point(painter.scope().width, u16::MAX));
        let mut msgs = Stack::south_east(vec![]);

        if let Some(conversation) = &model.conversation {
            for msg in conversation.messages.iter() {
                match msg {
                    crate::backend::types::ChatMessage::Message(msg) => {
                        let txt = Text::default()
                            .add(
                                StylishStringy::new(if msg.from_first_name.is_empty() {
                                    match ContactId::new(msg.from_id) {
                                        ContactId::SELF => Cow::Borrowed("Me"),
                                        _ => Cow::Owned(format!("({})", msg.from_id)),
                                    }
                                } else {
                                    Cow::Borrowed(msg.from_first_name.as_str())
                                })
                                .apply(
                                    if msg.from_id == ContactId::SELF.to_u32() {
                                        Style::default().front(Color::grey(false))
                                    } else {
                                        Style::default().front(Color::rgb(
                                            (msg.from_color / 256 / 256 % 255) as u8,
                                            (msg.from_color / 256 % 255) as u8,
                                            (msg.from_color % 255) as u8,
                                        ))
                                    },
                                ),
                            )
                            .add(": ")
                            .add(
                                msg.text
                                    .as_ref()
                                    .map(|t| t.as_str())
                                    .unwrap_or_default()
                                    .front(match ContactId::new(msg.from_id) {
                                        ContactId::SELF => Color::grey(false),
                                        _ => Color::grey(true),
                                    }),
                            );

                        msgs = msgs.add(txt);
                    }
                    crate::backend::types::ChatMessage::DayMarker(_) => {}
                }
            }
        }

        let scroll_painter = painter.with_scope(window(point(0, 0), scroll_screen.size()));
        let scroll_scope = msgs.update(&mut (), input, &mut scroll_screen, &scroll_painter);

        let scroll_offset = scroll_scope.height.saturating_sub(painter.scope().height);

        painter.fill(screen);
        painter.copy(
            scroll_screen.symbols().filter_map(|(pos, style, symbol)| {
                if pos.row >= scroll_offset {
                    Some((point(pos.col, pos.row - scroll_offset), style, symbol))
                } else {
                    None
                }
            }),
            screen,
        );

        painter.scope()
    }
}
