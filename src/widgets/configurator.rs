use crate::{app::command::CreateOrUpdateAccount, app::Dech, backend::types::ProfileConfig};
use termit::prelude::*;

/// Reduce cycling if not in use
pub struct Configurator<A: AppEvent> {
    ui: Box<dyn Widget<Dech, A>>,
}

impl<A: AppEvent> Default for Configurator<A> {
    fn default() -> Self {
        fn tt<A: AppEvent>(title: &str) -> impl Widget<Dech, A> + '_ {
            title.place().top(1).height(1)
        }
        fn tv<A: AppEvent>(
            accessor: impl Fn(&mut ProfileConfig) -> &mut String + 'static,
        ) -> impl Widget<Dech, A> {
            Canvas::new(TextBox::new(move |m: &mut Dech| {
                Some(accessor(m.profile_config_mut()))
            }))
            .front(Color::white())
            .back(Color::rgb(20, 20, 20))
            .place()
            .top(0)
            .height(1)
        }

        let ui = Box::new(
            Stack::south_east_boxes(vec![])
                .add_box(
                    Button::new(
                        Pretty::new("Save this profile configuration (click here when ready)")
                            .front(Color::blue(false)),
                    )
                    .on_click(|_w, m: &mut Dech| {
                        let cmd = CreateOrUpdateAccount {
                            config: m.profile_config_mut().clone(),
                        };
                        m.send_command(cmd);
                    }),
                )
                .add_box(tt("Display name"))
                .add_box(tv(|m: &mut ProfileConfig| &mut m.display_name))
                .add_box(tt("E-mail address"))
                .add_box(tv(|m: &mut ProfileConfig| &mut m.primary_address))
                .add_box(tt("IMAP server"))
                .add_box(tv(|m: &mut ProfileConfig| &mut m.mail_server))
                .add_box(tt("IMAP port"))
                .add_box(tv(|m: &mut ProfileConfig| &mut m.mail_port))
                .add_box(tt("IMAP user"))
                .add_box(tv(|m: &mut ProfileConfig| &mut m.mail_user))
                .add_box(tt("IMAP password"))
                .add_box(tv(|m: &mut ProfileConfig| &mut m.mail_password))
                .add_box(tt("SMTP server"))
                .add_box(tv(|m: &mut ProfileConfig| &mut m.send_server))
                .add_box(tt("SMTP port"))
                .add_box(tv(|m: &mut ProfileConfig| &mut m.send_port))
                .add_box(tt("SMTP user"))
                .add_box(tv(|m: &mut ProfileConfig| &mut m.send_user))
                .add_box(tt("SMTP password"))
                .add_box(tv(|m: &mut ProfileConfig| &mut m.send_password)),
        );

        Self { ui }
    }
}

impl<A: AppEvent> Widget<Dech, A> for Configurator<A> {
    fn update(
        &mut self,
        model: &mut Dech,
        input: &Event<A>,
        screen: &mut Screen,
        painter: &Painter,
    ) -> Window {
        self.ui.update(model, input, screen, painter)
    }
}
