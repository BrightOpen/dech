use crate::{
    app::command::{SaveDraft, SendDraft},
    app::Dech,
};
use termit::{
    prelude::*,
    widget::{Stack, TextBox, Widget, WidgetExt},
};

pub struct Composer<A: AppEvent> {
    /// Should it handle input?
    enabled: bool,
    ui: Stack<Vec<Box<dyn Widget<Dech, A>>>>,
}
impl<A: AppEvent> Default for Composer<A> {
    fn default() -> Self {
        Self {
            enabled: Default::default(),
            ui: Default::default(),
        }
    }
}

impl<A: AppEvent> Widget<Dech, A> for Composer<A> {
    fn update(
        &mut self,
        model: &mut Dech,
        mut input: &Event<A>,
        screen: &mut termit::Screen,
        painter: &termit::Painter,
    ) -> termit::geometry::Window {
        if self.ui.content().is_empty() {
            self.ui = Stack::east_south_boxes(vec![])
                .add_box("\u{27A1}\u{FE0F} ".pretty().load_with(|w, m: &Dech| {
                    let enabled = m
                        .conversation
                        .as_ref()
                        .map(|c| c.topic.can_send)
                        .unwrap_or_default();
                    w.style_mut().front_color = Some(if !enabled {
                        Color::red(false)
                    } else if !m.show_menu {
                        Color::cyan(true)
                    } else {
                        Color::grey(false)
                    })
                }))
                .add_box(
                    TextBox::new(|m: &mut Dech| m.draft().and_then(|d| d.message.text.as_mut()))
                        .load_with(|w, _m| w.active = true),
                );
        }

        if !self.enabled {
            // This prevents updating the textbox when not in chat mode
            input = &Event::Refresh(0)
        }
        // This prevents Enter on menu action from sending the draft
        self.enabled = !model.show_menu;

        let scope = self.ui.update(model, input, screen, painter);

        if let Event::Key(key) = input {
            if let Some(draft) = model.draft() {
                match key.keycode {
                    KeyCode::Enter => {
                        let cmd = SendDraft {
                            account_id: draft.account_id,
                            chat_id: draft.chat_id,
                            message: std::mem::take(draft.message),
                        };
                        model.send_command(cmd)
                    }
                    _ => {
                        let cmd = SaveDraft {
                            account_id: draft.account_id,
                            chat_id: draft.chat_id,
                            message: draft.message.clone(),
                        };
                        model.send_command(cmd)
                    }
                }
            }
        }

        scope
    }
}
