use crate::app::{Dech, DechEvent};
use termit::prelude::*;

/// Widget for DC main view allows switching the main view easily at run time
#[derive(Default)]
pub struct Main;

impl Widget<Dech, DechEvent> for Main {
    fn update(
        &mut self,
        model: &mut Dech,
        input: &Event<DechEvent>,
        screen: &mut Screen,
        painter: &Painter,
    ) -> Window {
        let ui = match model.main_preview {
            Some(ref mut preview) => preview,
            None => &mut model.main_view,
        };

        // cannot borrow model more than once mutably
        let mut ui = std::mem::replace(ui, Box::new(()));
        let scope = ui.update(model, input, screen, painter);

        // put it back
        if model.main_preview.is_some() {
            model.main_preview = Some(ui);
        } else {
            model.main_view = ui;
        }

        scope
    }
}
