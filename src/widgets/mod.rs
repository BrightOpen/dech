mod action;
mod app_events;
mod chat;
mod commander;
mod composer;
mod configurator;
mod main;
mod nice;

pub use action::*;
pub use app_events::*;
pub use chat::*;
pub use commander::*;
pub use composer::*;
pub use configurator::*;
pub use main::*;
pub use nice::*;

use self::composer::Composer;
use crate::app::{Dech, DechEvent};
use termit::prelude::*;

pub fn make_ui() -> impl Widget<Dech, DechEvent> {
    Canvas::with_style(
        Style::DEFAULT.back(Color::grey(false)).back(Color::black()),
        Stack::south_east_boxes(vec![])
            .add_box(Nice::default())
            .add_box(AppEvents)
            .add_box(
                Commander::default()
                    .place()
                    .left(0)
                    .top(0)
                    .load_with(|w, m| {
                        w.placement =
                            w.placement
                                .width(if m.show_menu { m.menu_page_width } else { 0 })
                    }),
            )
            .add_box(Main.place().top(1).left(2).right(2).bottom(1)),
    )
}

pub fn make_ui_main_view_welcome() -> impl Widget<Dech, DechEvent> {
    "Hi, this is Dech, a command line delta.chat client.\r\n\
    \r\n\
    The menu on the left shows and hides with the Esc key. It shows \
    available actions and you can filter/search them if you type \
    some text. It supports mouse - clicking, scrolling, \
    and selecting with Alt. You can also navigate it with \
    Up and Down or PageUp and PageDown keys. The search filter you \
    type can be removed with Backspace."
}

pub fn make_ui_main_view_conversation_action_preview(
    action: impl Widget<Dech, DechEvent> + 'static,
) -> impl Widget<Dech, DechEvent> {
    Stack::south_east_boxes(vec![])
        .add_box(
            // header
            Canvas::new(String::default())
                .front(Color::blue(false))
                .load_with(move |w, m: &Dech| {
                    *w.content_mut() = m
                        .conversation
                        .as_ref()
                        .map(|conv| conv.topic.name.clone())
                        .unwrap_or_default()
                })
                .place()
                .top(0)
                .height(1),
        )
        .add_box(action.place().top(1))
        .add_box(
            "No conversation"
                .to_owned()
                .load_with(move |w, m: &Dech| {
                    *w = m
                        .conversation
                        .as_ref()
                        .map(|conv| {
                            let topic = &conv.topic;
                            format!(
                                "{}: {}\nUnread: {}\nId: {}\nDebug: {:#?}",
                                if topic.header.is_empty() {
                                    if topic.chat_type == "Single" {
                                        topic.name.as_str()
                                    } else {
                                        "-"
                                    }
                                } else {
                                    topic.header.as_str()
                                },
                                topic.preview,
                                topic.fresh_msg_cnt,
                                topic.id,
                                topic
                            )
                        })
                        .unwrap_or_else(|| "No conversation".to_owned())
                })
                .place()
                .top(1),
        )
}

pub fn make_ui_main_view_conversation_preview<A: AppEvent>(
    chat_id: u32,
    action: impl Widget<Dech, A> + 'static,
) -> impl Widget<Dech, A> {
    Stack::south_east_boxes(vec![])
        .add_box(
            // header
            Canvas::new(String::default())
                .front(Color::blue(false))
                .load_with(move |w, m: &Dech| {
                    *w.content_mut() = m
                        .session
                        .as_ref()
                        .and_then(|acc| acc.topics.iter().filter(move |t| t.id == chat_id).next())
                        .map(|topic| topic.name.clone())
                        .unwrap_or_default()
                })
                .place()
                .top(0)
                .height(1),
        )
        .add_box(action.place().top(1))
        .add_box(
            "No conversation"
                .to_owned()
                .load_with(move |w, m: &Dech| {
                    *w = m
                        .session
                        .as_ref()
                        .and_then(|acc| acc.topics.iter().filter(move |t| t.id == chat_id).next())
                        .map(|topic| {
                            format!(
                                "{}: {}\nUnread: {}\nId: {}\nDebug: {:#?}",
                                if topic.header.is_empty() {
                                    if topic.chat_type == "Single" {
                                        topic.name.as_str()
                                    } else {
                                        "-"
                                    }
                                } else {
                                    topic.header.as_str()
                                },
                                topic.preview,
                                topic.fresh_msg_cnt,
                                topic.id,
                                topic
                            )
                        })
                        .unwrap_or_else(|| "No conversation".to_owned())
                })
                .place()
                .top(1),
        )
}

pub fn make_ui_main_view_account_action_preview<A: AppEvent>(
    action: impl Widget<Dech, A> + 'static,
) -> impl Widget<Dech, A> {
    Stack::south_east_boxes(vec![])
        .add_box(
            // header
            Canvas::new(String::default())
                .front(Color::blue(false))
                .load_with(|w, m: &Dech| {
                    *w.content_mut() = m
                        .session
                        .as_ref()
                        .map(|acc| {
                            acc.profile
                                .display_name
                                .as_ref()
                                .unwrap_or(&acc.profile.email)
                                .clone()
                        })
                        .unwrap_or_default()
                })
                .place()
                .top(0)
                .height(1),
        )
        .add_box(action.place().top(1))
        .add_box(
            "No account"
                .to_owned()
                .load_with(|w, m: &Dech| {
                    *w = match &m.session {
                        Some(acc) => format!(
                            "Status: {:?}\r\nEmail: {}\r\nId: {}",
                            acc.profile.logged_in, acc.profile.email, acc.profile.id
                        ),
                        None => "No account".to_owned(),
                    }
                })
                .place()
                .top(1),
        )
}

pub fn make_ui_main_view_account_preview(
    account_id: u32,
    action: impl Widget<Dech, DechEvent> + 'static,
) -> impl Widget<Dech, DechEvent> {
    Stack::south_east_boxes(vec![])
        .add_box(
            // header
            Canvas::new(String::default())
                .front(Color::blue(false))
                .load_with(move |w, m: &Dech| {
                    *w.content_mut() = m
                        .profiles
                        .iter()
                        .filter(move |acc| acc.id == account_id)
                        .map(|acc| acc.display_name.as_ref().unwrap_or(&acc.email).clone())
                        .next()
                        .unwrap_or_default()
                })
                .place()
                .top(0)
                .height(1),
        )
        .add_box(action.place().top(1))
        .add_box(
            "No account"
                .to_owned()
                .load_with(move |w, m: &Dech| {
                    *w = m
                        .profiles
                        .iter()
                        .filter(move |acc| acc.id == account_id)
                        .map(|acc| {
                            format!(
                                "Status: {:?}\r\nEmail: {}\r\nId: {}",
                                acc.logged_in, acc.email, acc.id
                            )
                        })
                        .next()
                        .unwrap_or_default()
                })
                .place()
                .top(1),
        )
}

pub fn make_ui_main_view_conversation<A: AppEvent>() -> impl Widget<Dech, A> {
    Stack::south_east_boxes(vec![])
        .add_box(
            // chat name
            Canvas::new(String::default())
                .front(Color::blue(false))
                .load_with(|w, m: &Dech| {
                    *w.content_mut() = m
                        .conversation
                        .as_ref()
                        .map(|chat| chat.topic.name.clone())
                        .unwrap_or_default()
                })
                .place()
                .top(0)
                .height(1),
        )
        .add_box(Chat::default().place().top(1).bottom(2))
        .add_box(Composer::default().place().height(1).bottom(0))
}
