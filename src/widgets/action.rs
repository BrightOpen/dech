use std::fmt::Debug;
use termit::prelude::*;

pub struct Action<M> {
    pub description: String,
    pub on_select: Box<dyn Fn(&mut M)>,
    pub on_action: Box<dyn Fn(&mut M)>,
    pub style: Style,
    pub hidden: Box<dyn Fn(&Action<M>, &M) -> bool>,
    pub selected: bool,
    pub area: Window,
}
impl<M> Default for Action<M> {
    fn default() -> Self {
        Self {
            description: Default::default(),
            on_select: Box::new(|_| {}),
            on_action: Box::new(|_| {}),
            style: Default::default(),
            hidden: Box::new(|_, _| false),
            selected: false,
            area: Default::default(),
        }
    }
}
impl<M> Action<M> {
    pub fn new(description: impl ToString, style: Style) -> Self {
        Self {
            description: description.to_string(),
            style,
            ..Default::default()
        }
    }
    pub fn on_action(mut self, action: impl Fn(&mut M) + 'static) -> Self {
        self.on_action = Box::new(action);
        self
    }
    pub fn on_select(mut self, selection: impl Fn(&mut M) + 'static) -> Self {
        self.on_select = Box::new(selection);
        self
    }
    pub fn hide_when(mut self, hide: fn(&Action<M>, &M) -> bool) -> Self
    where
        Self: 'static,
    {
        self.hidden = Box::new(move |a, m| hide(a, m) || (self.hidden)(a, m));
        self
    }

    pub fn action(&self, model: &mut M) {
        (self.on_action)(model)
    }

    pub fn select(&self, model: &mut M) {
        (self.on_select)(model)
    }
}
impl<M> Debug for Action<M> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Action")
            .field("description", &self.description)
            .field("style", &self.style)
            .finish()
    }
}

impl<M, A:AppEvent> Widget<M, A> for Action<M> {
    fn update(
        &mut self,
        model: &mut M,
        _input: &Event<A>,
        screen: &mut termit::Screen,
        painter: &termit::Painter,
    ) -> termit::geometry::Window {
        if (self.hidden)(self, model) {
            // take 0 space
            return window(painter.scope().position(), point(0, 0));
        }

        // \U1FB6D 🭬
        // \U1FB6E 🭮
        let mut text = self.description.clone();

        text += &" ".repeat((painter.scope().width as usize).saturating_sub(text.len()));

        let mut bg = Color::rgb(20, 20, 20);
        if self.selected {
            bg = Color::rgb(10, 10, 10);
        }
        self.area = painter
            .apply(self.style.back(bg))
            .paint(text.as_str(), screen, 0, false);

        self.area
    }
}
