# dech

A simplistic [Delta.Chat](https://delta.chat) client inspired by [the dreamer project](https://github.com/dignifiedquire/dreamer).

- [x] Show and select accounts
- [x] Show and select chats
- [x] Show chat message text (no status or type or attachments)
- [x] Quick reply editor - single line
- [x] Send quick reply message 
- [x] Simple account management (new, modify, delete)
- [ ] Contact management
- [ ] Chats management (new, delete)
- [ ] Message management (delete old)
- [ ] Scroll message history
- [ ] Attachments
- [ ] Message state
- [ ] Search in all chats
- [ ] Advanced account management (export, import, backup, restore)

For missing features you may use the delta.chat desktop app. It shares the profile.


## License

Licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or
  http://www.apache.org/licenses/LICENSE-2.0)
- MIT License ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
